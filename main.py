from stock_price_prediction import main
from flask import Flask, request, jsonify
from stock_price_prediction.model.preprocessing import pre_processing, get_date
from stock_price_prediction import plot

app = Flask(__name__)


def check_fields(data):
    return data['open'] and data['high'] and data['low'] and data['volume']


@app.route("/", methods=['GET'])
def hello_world():
    return "Hello, this is Parallel Computing Assignment"


@app.route('/predict', methods=['GET'])
def _prediction():
    data = request.get_json()
    print(data)
    if not check_fields(data):
        return jsonify({"error": 400, "data": "Missing required fields"})
    _input = [data['open'], data['high'], data['low'], data['volume']]
    y_pred = main.predict(_input)
    return jsonify({"error": 200, "data": str(y_pred[0][0])})


@app.route("/train", methods=["GET"])
def _training():
    main.remove_trained_model()
    train_loss, test_loss = main.train()
    return jsonify({"error": 200, "data": {"train_loss": str(train_loss[-1]), "test_loss": str(test_loss[-1])}})


def plot_pred():
    x_train, y_train, x_test, y_test = pre_processing()
    y_train_pred = main.predict_normalized(x_train)
    y_test_pred = main.predict_normalized(x_test)
    date = get_date()
    plot.plot(date=date, y_train=y_train, y_test=y_test,
              y_train_pred=y_train_pred, y_test_pred=y_test_pred)


def train_then_plot_loss():
    main.remove_trained_model()
    train_loss, test_loss = main.train()
    plot.plot_loss(train_loss=train_loss, test_loss=test_loss)


if __name__ == '__main__':
    app.run(host='0.0.0.0')

# Parallel Computing Assignment

## Problem 1: Credit Card Fraud Detection

#### Sources

[Kaggle](https://www.kaggle.com/mlg-ulb/creditcardfraud)

### Algorithm

We choose Decision Tree to solve this problem.

**Source:** [Kaggle Decision Tree](https://www.kaggle.com/jab8en/fraud-prediction-based-on-decision-trees)

## Problem 2: Stock Price Prediction

#### References

[Kaggle](https://www.kaggle.com/rosand/fork-of-predict-stock-prices-with-svm)

#### Data

Data is collected on [Vietstock](https://finance.vietstock.vn/ket-qua-giao-dich)

### Algorithm

We choose Support Vector Regression to solve this problem.

## Docker: things to learn and do

1. Dockerfile
2. Docker-compose

## Project Structure

1. Each folder is a problem need to be solved. Each problem has a folder named 'data' contains a csv file, 'model' contains the algorithm solving the problem written in python3.
2. Each problem has a docker-compose.yml or a Dockerfile or both.
3. Run each problem separately (in its folder) using docker.

## Work Assignment

- Problem 1: Anh, Duyen
- Problem 2: Huy, Duy

# Rules

1. DO NOT CHANGE others' code, ONLY change your code.
2. INFORM team member right after pushing.
3. IF FIXING CONFLICTS REQUIRES CHANGING others' CODE, discuss with others before fixing.
4. EACH PROBLEM is solve on a SEPARATE BRANCH, for example, brach 'credit-card' is for problem 1. After finished, create MERGE REQUEST from the working branch to master.
5. MASTER can not be pushed, so do not try to push to master :)

# Deadline

20:00 (8:00 PM) on Saturday, 18/5/2019

# API Docs

### Prediction

API: /predict

METHOD: GET

Input:

```json
{
  "open": 45000,
  "high": 49000,
  "low": 44000,
  "volume": 254000
}
```

Successful output:

```json
{
  "error": 200,
  "data": 43209.2
}
```

Failed output:

```json
{
  "error": 400,
  "data": "Missing fields"
}
```

### Training

API: /train

METHOD: GET

Input: None

Output:

```json
{
  "error": 200,
  "data": {
    "train_loss": 0.13,
    "test_loss": 0.05
  }
}
```
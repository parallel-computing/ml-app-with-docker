from joblib import load
from .model import svr_tensorflow as svr_tensor
import numpy as np
import os, shutil

train_model_path = os.path.dirname(os.path.abspath(__file__)) + "/trained-models"


def predict(_input):
    scaler_x = load("./stock_price_prediction/model/scaler_x")
    scaler_y = load("./stock_price_prediction/model/scaler_y")
    _input = np.array([_input])
    _input = scaler_x.transform(_input.astype(float))
    ret = svr_tensor.predict(_input)
    return scaler_y.inverse_transform(ret)


def predict_normalized(_input):
    scaler_y = load("./stock_price_prediction/model/scaler_y")
    ret = svr_tensor.predict(_input)
    return scaler_y.inverse_transform(ret)

def remove_trained_model():
    print(train_model_path)
    shutil.rmtree(train_model_path)


def train():
    return svr_tensor.main()

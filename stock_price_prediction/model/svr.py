from .preprocessing import pre_processing
from sklearn.svm import SVR
from joblib import dump, load
import time
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_squared_error


def train():
    start = time.time()
    # Extract features
    _x_train, _y_train, _x_test, _y_test = pre_processing()
    # Init model
    model = SVR(kernel='rbf', gamma='scale', C=500, epsilon=0.0000005)
    # Start training
    model.fit(_x_train, _y_train)
    # Save trained model
    dump(model, 'trained-svr.joblib')
    end = time.time()
    print("Training finished in " + str(end - start) + " seconds")
    return model


def test():
    _x_train, _y_train, _x_test, _y_test = pre_processing()
    # load model
    model = load('trained-svr.joblib')
    return model.predict(_x_test)


def validate():
    _x_train, _y_train, _x_test, _y_test = pre_processing()
    model = load('trained-svr.joblib')
    _y_pred_train = model.predict(_x_train)
    _y_pred_test = model.predict(_x_test)
    return mean_squared_error(_y_train, _y_pred_train), mean_squared_error(_y_test, _y_pred_test)


def plot():
    _x_train, _y_train, _x_test, _y_test = pre_processing()
    model = load('trained-svr.joblib')
    plt.figure(figsize=(16, 9))
    index_train = [x for x in range(len(_x_train))]
    index_test = [x for x in range(len(_x_train), len(_x_train) + len(_x_test))]
    index = index_train + index_test
    close_data = np.append(_y_train, _y_test)
    _y_pred_train = model.predict(_x_train)
    _y_pred_test = model.predict(_x_test)
    #plt.plot(index, close_data, 'b')
    plt.plot(index_test, _y_test, 'b')
    plt.plot(index_test, _y_pred_test, 'r')
    #plt.plot(index_train, _y_pred_train, 'g')
    plt.show()


def predict(features):
    # load model
    model = load('trained-svr.joblib')
    # predict
    return model.predict(features)
